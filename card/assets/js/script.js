$(function() {

    var owner = $('#owner');
    var cardNumber = $('#cardNumber');
    var cardNumberField = $('#card-number-field');
    var CVV = $("#cvv");
    var mastercard = $("#mastercard");
    var confirmButton = $('#confirm-purchase');
    var visa = $("#visa");
    var amex = $("#amex");

    // Use the payform library to format and validate
    // the payment fields.

    cardNumber.payform('formatCardNumber');
    CVV.payform('formatCardCVC');


    cardNumber.keyup(function() {

        amex.removeClass('transparent');
        visa.removeClass('transparent');
        mastercard.removeClass('transparent');

        if ($.payform.validateCardNumber(cardNumber.val()) == false) {
            cardNumberField.addClass('has-error');
        } else {
            cardNumberField.removeClass('has-error');
            cardNumberField.addClass('has-success');
        }

        if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
            mastercard.addClass('transparent');
            amex.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
            mastercard.addClass('transparent');
            visa.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
            amex.addClass('transparent');
            visa.addClass('transparent');
        }
    });

    confirmButton.click(function(e) {

        e.preventDefault();

        var isCardValid = $.payform.validateCardNumber(cardNumber.val());
        var isCvvValid = $.payform.validateCardCVC(CVV.val());

        if(owner.val().length < 5){
            alert("Verifique o  Nome");
        } else if (!isCardValid) {
            alert("Verifique o numero do cartão");
        } else if (!isCvvValid) {
            alert("Verifique o CVV");
        } else {




 var serializeDados = $('#formulario_novo').serialize();



        $.ajax({
                 type: "POST",
                 url: "salvar_cartao.php",
                 data: serializeDados,
                
                 success: function (result) {

                  

                   

                    


                  toastr.success('Gravado com succeso');

                    $('#id_cartao').val(result);
                   $( "#pagamento" ).hide( "blind", { direction: "down" }, "slow" );
                   $( "#dados_pagamento" ).show( "blind", { direction: "down" }, "slow" );








                    
                 }
             });
             return false; // required to block normal submit since you used ajax




        }
    });
});
