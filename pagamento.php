
<?php
session_start();


  

//PEGA OS DADOS DO SEU SISTEMA DE PREFERENCIA PUXANDO PELO ID DA COMPRA OS DADOS.
  $_SESSION['nome']            = '';
  $_SESSION['valor']           = '';
  $_SESSION['id_auto']         = '';
  $_SESSION['data_vencimento'] = '';
  $_SESSION['Valor_de_Pagto']  = '';



    //TOKEM PAGSEGURO
    $data['token'] ='28869FFAE53747D4A9C0713DC9F4A35C'; 
    //E-MAIL PAGSEGURO
    $emailPagseguro = "financeiro@movidoaweb.com.br";
    $data = http_build_query($data);
    $url = 'https://ws.pagseguro.uol.com.br/v2/sessions';
    $curl = curl_init();
    $headers = array('Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1'
      );
    curl_setopt($curl, CURLOPT_URL, $url . "?email=" . $emailPagseguro);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $curl,CURLOPT_RETURNTRANSFER, true );
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    //curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $xml = curl_exec($curl);
    curl_close($curl);
    $xml= simplexml_load_string($xml);
    $idSessao = $xml -> id;
    
   


   //DADOS DA SUA VENDA 
    $id_venda ='100';
    $valor = '50.50';
    $data_vencimento ='10/10/2020';

    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="CHECKOUT TRANSPARENTE">
    <meta name="author" content="LUCAS GHILARDI - Movido a Web">
    <meta name="keyword" content="Sistema">
    <link href="assets/css/toastr.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    

   
        <script src="assets/js/pagSeguro.js"></script>

<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

<script type="text/javascript">


 PagSeguroDirectPayment.setSessionId('<?=$idSessao?>');
</script>


  <?php


   include('include/html_cima.php'); ?>




  </head>

  <body>

  <section id="container">
      <!--header start-->
        <?php include('include/header.php'); ?>

      <!--header end-->
      <!--sidebar start-->
     <?php include('include/menu2.php'); ?>

      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
             
              <!--state overview end-->
              
                  
              <div class="row">
                  
                  <div class="col-lg-12">                  
                      
                          <div class="form-row">
                              
                            <div class="form-group col-md-12">
                              
                              <div class="creditCardForm" name='pagamento' id='pagamento'>
                                <div class="heading">
                                    <h1>Cartão de Crédito</h1>
                                </div>
                                <div class="payment">
                                    <form id="formulario_novo" name="formulario_novo">
                                        <div class="form-group owner">
                                            <label for="owner">Nome Completo</label>
                                            <input type="text" class="form-control" id="owner" name="nome">
                                        </div>
                                        <div class="form-group CVV">
                                            <label for="cvv">CVV</label>
                                            <input type="text" class="form-control" id="cvv" name="cvv">
                                        </div>
                                        <div class="form-group" id="card-number-field">
                                            <label for="cardNumber">Numero do Cartão</label>
                                            <input type="text" class="form-control" id="cardNumber" name="cardNumber">
                                        </div>
                                        <div class="form-group" id="expiration-date">
                                            <label>Expiraçao Date</label>
                                            <select name="mes" id='mes'>
                                                <option value="01">Janeiro</option>
                                                <option value="02">Fevereiro </option>
                                                <option value="03">Março</option>
                                                <option value="04">Abril</option>
                                                <option value="05">Maio</option>
                                                <option value="06">Junho</option>
                                                <option value="07">Julho</option>
                                                <option value="08">Agosto</option>
                                                <option value="09">Setembro</option>
                                                <option value="10">Outubro</option>
                                                <option value="11">Novembro</option>
                                                <option value="12">Dezembro</option>
                                            </select>
                                            <select name="ano" id='ano'>
                                                
                                               
                                                <option value="2020"> 2020</option>
                                                <option value="2021"> 2021</option>
                                                <option value="2022"> 2022</option>
                                                <option value="2023"> 2023</option>
                                                <option value="2024"> 2024</option>
                                                <option value="2025"> 2025</option>
                                                <option value="2026"> 2026</option>
                                                <option value="2027"> 2027</option>
                                                <option value="2028"> 2028</option>
                                                <option value="2029"> 2029</option>
                                                <option value="2030"> 2030</option>
                                                <option value="2030"> 2031</option>
                    
                                            </select>
                                        </div>
                                        <div class="form-group" id="credit_cards">
                                            <img src="card/assets/images/visa.jpg" id="visa">
                                            <img src="card/assets/images/mastercard.jpg" id="mastercard">
                                            <img src="card/assets/images/amex.jpg" id="amex">
                                        </div>
                                        <div class="form-group" id="pay-now">
                                            <button type="submit" class="btn btn-default confirmaaa" id="confirm-purchase">Confirmar</button>
                                        </div>
                                         <div class="form-group">

											                     Valor  <button type="submit" class="btn btn-default" disabled="true"><?=$valor?></button>
                                         </div>
                                          <div class="form-group">

										                          	Vencimento  <button type="submit" class="btn btn-default" disabled="true"><?=$data_vencimento?></button>
                                         </div>

                                
                                        <input type="hidden" name='idd' value="<?=$id_venda?>">
                                    </form>
                                </div>
                            </div><!--credit-form-->
                    
                             <!--inicio segundo-->
                               
                           <div class="col-lg-12"  name='dados_pagamento' id='dados_pagamento'  style="display:none ">
                    
                    
                            <section class="card">
                              
                                      <header class="card-header">
                                         Dados do Titular  do Cartão
                                      </header>
                                      
                                      <div class="card-body">
            
                                          <form class="form tasi-form" method="get" name="formulario2" id='formulario2'>
                                          	    <input type="hidden" name="hash" value="<?=$idSessao?>">
											                          <input type="hidden" class="hashPagSeguro" name="hashPagSeguro" id='hashPagSeguro'>
											                          <input type="hidden" name="creditCardToken" class="tokenPagamentoCartao" id='tokenPagamentoCartao'>
                                              <div class="row">
                                                  
                                                  <div class="col-lg-12">
                                                      
                                                      <div class="form-group">
                                                          <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail</label>                                                          
                                                          <input type="email"  name="senderEmail" id="senderEmail" class="form-control" required="required" value="<?=$email?>">
                                                          
                                                      </div>
                                                      
                                                   </div>   
                                                  
                                              </div><!--row-->
                                              
                                              <div class="row">
                                                  
                                                  <div class="col-lg-6">
                                                  
                                                      <div class="form-group">
                                                          
                                                          <label for="nomecompleto"><i class="fa fa-user" aria-hidden="true" ></i>  Nome Completo</label>
                                                          <input type="text" name="senderName" id="senderName" required="required" class="form-control" value="<?=$nome?>" >
                                                          
                                                      </div>
                                                      
                                                  </div>
                                                  
                                                  <div class="col-lg-6">
                                                      
                                                      <div class="form-group">
                                                          <label for="datanasc"><i class="fa fa-calendar" aria-hidden="true"></i> Data Nascimento</label>
                                                          
                                                              <input type="text" name="creditCardHolderBirthDate" id="creditCardHolderBirthDate" required="required" class="form-control" value="<?=$data_nascimento?>">
                                                          
                                                      </div>
                                                      
                                                   </div>
                                                   
                                              </div><!--roow-->
                                              
                                              <div class="row">
                                                  
                                                  <div class="col-lg-6">
                                                  
                                                      <div class="form-group">
                                                          
                                                          <label for="cpf"><i class="fa fa-check-square-o" aria-hidden="true"></i> CPF</label>
                                                      
                                                          <input  name="senderCPF" id="senderCPF" maxlength="14" class="form-control cpf" required="required" value="<?=$cpf?>" type='text'>
                                                           <small id="emailHelp" class="form-text text-muted">Somente Números</small>
                                                      </div>
                                                   
                                                   </div>
                                                  
                                                   <div class="col-lg-6">
                                                       
                                                    <div class="form-group">
                                                      
                                                      <label for="telefone"><i class="fa fa-phone-square" aria-hidden="true"></i> Telefone</label>
                                                      
                                                      <input type="text" name="senderPhone" id="senderPhone" class="phone form-control fone" maxlength="12"  required="required" value="<?=$telefone?>" >
                                                      </div>
                                                      
                                                  </div>
                                                  
                                              </div><!--roww-->
                                              
                                              <div clas="row">
                                                  
                                                  <div class="col-lg-3">
                                                  
                                                      <div class="form-group">
                                                          
                                                          <label for="cep"><i class="fa fa-globe" aria-hidden="true"></i> CEP</label>
                                                          
                                                          <input type="text" name="shippingAddressPostalCode" id="shippingAddressPostalCode" class="form-control cep" maxlength="9" required="required" value="<?=$cep?>" >
                                                          
                                                      </div>
                                                      
                                                  </div>
                                                      
                                               </div><!--roww-->
                                               
                                               
                                               
                                               <div class="row">
                                                   
                                                   <div class="col-lg-4">

                                                        <div class="form-group">
                                                            
                                                            <label for="endereco"><i class="fa fa-map-marker" aria-hidden="true"></i> Endereço</label>
                                                      
                                                            <input  type="text" name="shippingAddressStreet" id="shippingAddressStreet" class="form-control" required="required" value="<?=$endereco?>">
                                                      
                                                      </div>
                                                  
                                                  </div>
                                                  
                                                  <div class="col-lg-4">
                                                      
                                                      <div class="form-group">
                                                           
                                                           <label for="nuemro"><i class="fa fa-map-marker" aria-hidden="true"></i> Número</label>
                                                          
                                                           <input type="text" name="shippingAddressNumber" id="shippingAddressNumber" class="form-control" size="5" required="required">
                                                      
                                                        </div>
                                                      
                                                  </div>
                                                  
                                                  <div class="col-lg-4">
                                                      
                                                       <div class="form-group">
                                                           
                                                           <label for="comp"><i class="fa fa-map-marker" aria-hidden="true"></i> Complemento</label>
                                                          
                                                           <input type="text" name="shippingAddressComplement" id="shippingAddressComplement" class="form-control" >
                                                          
                                                      </div>
                                                      
                                                  </div>
                                                                                                    
                                              </div><!--roww-->
                                              
                                              <div class="row">
                                                  
                                                  <div class="col-lg-4">
                                                      
                                                      <div class="form-group">
                                                           
                                                          <label for="bairro"><i class="fa fa-location-arrow" aria-hidden="true"></i> Bairro</label>
                                                          
                                                          <input type="text" name="shippingAddressDistrict" id="shippingAddressDistrict" required="required" value="<?=$bairro?>" class="form-control">
                                                          
                                                      </div>
                                                      
                                                  </div>
                                                  
                                                  <div class="col-lg-4">
                                                      
                                                      <div class="form-group">
                                                       
                                                         <label for="cidade"><i class="fa fa-location-arrow" aria-hidden="true"></i> Cidade</label>
                                                            
                                                         <input type="text" name="shippingAddressCity" id="shippingAddressCity" required="required" value="<?=$cidade?>" class="form-control">
                                                         
                                                      </div>
                                                      
                                                  </div>
                                                      
                                                  <div class="col-lg-4">
                                                      
                                                      <div class="form-group">
                                                          
                                                          <label for="estado"><i class="fa fa-location-arrow" aria-hidden="true"></i> Estado</label>
                                                          
                                                          <input type="text" name="shippingAddressState" id="shippingAddressState" class="addressState form-control"  maxlength="2" style="text-transform: uppercase;" onBlur="this.value=this.value.toUpperCase()" required="required" value="<?=$estado?>">
                                                          
                                                      </div>
                                                      
                                                  </div>
                                                  
                                              </div><!--row-->
                                              
                                            <input type="hidden" name="registro" value="<?=$Registro?>">
                
                                            <input type="hidden" name="id_cartao" id='id_cartao'>
                                            <input type="submit" class="btn btn-success" value="Salvar" id='salvar22' >



                                                 <div id='resultadoq'></div>
                                                                    
                                            </div><!--12-->
                    
                                          </form>


                                          
                                          
                              </div><!--12-->
                                       <div class="col-lg-12"  name='dados_pagamento2' id='dados_pagamento2'  style="display:none ">
                    
                    
                                                <section class="card">
                              
                                                    <header class="card-header" <?=$css?>>
                                                     <center> <h2>Obrigado</h1> </center>
                                                    </header>
                                                      <div class="card-body">
                                                    
                                                    <center>Pagamento em Analise <br>
                                                             Você recebera por e-mail informaçoes sobre pagamento assim for processado.
                                                  </center>
                                          
                                                </section>
                                          </div>
            
                            </div><!--form-group-12-->
                               
                        </div><!--12-->
                     
                    </div>  <!--col-->
    
                    <br><br>
    
                  </div> <!--row-->
    
              </section>
          </section>
          <!--main content end-->

      <!-- Right Slidebar start -->
   
     <?php include('include/foolter.php');?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="html/js/bootstrap.bundle.min.js"></script>
    <script class="include" type="text/javascript" src="html/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="html/js/jquery.scrollTo.min.js"></script>
    <script src="html/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="html/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="html/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="html/js/owl.carousel.js" ></script>
    <script src="html/js/jquery.customSelect.min.js" ></script>
    <script src="html/js/respond.min.js" ></script>
        <script src="html/js/dist/jquery.validate.js" ></script>


    <!--right slidebar-->
    <script src="html/js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="html/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="html/js/sparkline-chart.js"></script>
    <script src="html/js/easy-pie-chart.js"></script>
        <script src="html/assets/toastr-master/toastr.js"></script>


      <script type="text/javascript" src="html/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>


  <script>

      //owl carousel

      $(document).ready(function() {
         





toastr.options = {
  "closeButton": true,
  "debug": false,
  "progressBar": false,
  "positionClass": "toast-top-center",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

$(".cep").mask("99999-999");
$(".fone").mask("(99)999999999");
$(".cpf").mask("999.999.999-99");
$("#creditCardHolderBirthDate").mask("99/99/9999");



 $("#shippingAddressPostalCode").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#shippingAddressStreet").val("...");
                        $("#shippingAddressDistrict").val("...");
                        $("#shippingAddressCity").val("...");
                        $("#shippingAddressState").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#shippingAddressStreet").val(dados.logradouro);
                                $("#shippingAddressDistrict").val(dados.bairro);
                                $("#shippingAddressCity").val(dados.localidade);
                                $("#shippingAddressState").val(dados.uf);
                                $("#shippingAddressNumber").focus();
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });




$("#formulario2").submit(function(){


 var serializeDados = $('#formulario2').serialize();



 $.ajax({
                 type: "POST",
                 url: "pagamento_avulso_pagseguro.php",
                 data: serializeDados,
                
                 success: function (result) {

                  

             
                   $( "#dados_pagamento" ).hide( "blind", { direction: "down" }, "slow" );
                

                     $('#resultadoq').html(result);
                      $( "#dados_pagamento2" ).show( "blind", { direction: "down" }, "slow" );
                    

                  toastr.success('Realizado com Sucesso');






                    
                 }
             });
             return false; // required to block normal submit since you used ajax

});








      });

      //custom select box

     

  </script>
    <script src="card/assets/js/jquery.payform.min.js" charset="utf-8"></script>
    <script src="script_pagamento.js"></script>
  </body>
</html>
