<?php


$notificationCode = preg_replace('/[^[:alnum:]-]/','',$_POST["notificationCode"]);


//PODE TESTAR PEGANDO O NOTIFICATIONCODO NO PAGSEGURO
//$notificationCode="78C4A2-6FD49CD49C5B-9664F30FB270-9542A2";


$data['token'] ='';
$data['email'] = '';

$data = http_build_query($data);

$url = 'https://ws.pagseguro.uol.com.br/v3/transactions/notifications/'.$notificationCode.'?'.$data;

$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_URL, $url);
$xml = curl_exec($curl);
curl_close($curl);

$xml = simplexml_load_string($xml);


$status=$xml->status;
$refencia=$xml->reference;
$valor_pago=$xml->grossAmount;

//ABERTURA - A
$verifica=substr($refencia, 0,1);


//PAGAMENTO AVULSO
if ($verifica=="V")
{



echo $refencia=str_replace("V", "", $refencia);
echo "<br>";



$dados['id']=$refencia;
$dados['notificationCode']=$notificationCode;

$dados['status']=$status;


echo "REFERENCIA".$refencia;
echo "STATUS". $status;
echo "Valor Pago". $valor_pago;

$dados['valor_pago']=$valor_pago;


}




/* Código	Significado
1	Aguardando pagamento: o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.
2	Em análise: o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.
3	Paga: a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.
4	Disponível: a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.
5	Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.
6	Devolvida: o valor da transação foi devolvido para o comprador.
7	Cancelada: a transação foi cancelada sem ter sido finalizada.

*/

?>