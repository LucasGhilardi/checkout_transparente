$(function() {

    var owner = $('#owner');
    var cardNumber = $('#cardNumber');
    var cardNumberField = $('#card-number-field');
    var CVV = $("#cvv");
    var mastercard = $("#mastercard");
    var confirmButton = $('#confirm-purchase');
    var visa = $("#visa");
    var amex = $("#amex");

    // Use the payform library to format and validate
    // the payment fields.

    cardNumber.payform('formatCardNumber');
    CVV.payform('formatCardCVC');


    cardNumber.keyup(function() {

        amex.removeClass('transparent');
        visa.removeClass('transparent');
        mastercard.removeClass('transparent');

        if ($.payform.validateCardNumber(cardNumber.val()) == false) {
            cardNumberField.addClass('has-error');
        } else {
            cardNumberField.removeClass('has-error');
            cardNumberField.addClass('has-success');
        }

        if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
            mastercard.addClass('transparent');
            amex.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
            mastercard.addClass('transparent');
            visa.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
            amex.addClass('transparent');
            visa.addClass('transparent');
        }
    });

    confirmButton.click(function(e) {

        e.preventDefault();

        var isCardValid = $.payform.validateCardNumber(cardNumber.val());
        var isCvvValid = $.payform.validateCardCVC(CVV.val());

        if(owner.val().length < 5){
            alert("Verifique o  Nome");
        } else if (!isCardValid) {
            alert("Verifique o numero do cartão");
        } else if (!isCvvValid) {
            alert("Verifique o CVV");
        } else {


            numCartao = $("#cardNumber").val();


        numCartao = numCartao.replace(" ", "");
        numCartao = numCartao.replace(" ", "");
        numCartao = numCartao.replace(" ", "");
       
             cvvCartao = $("#cvv").val();
        expiracaoMes = $("#mes").val();
        expiracaoAno = $("#ano").val();


     

         PagSeguroDirectPayment.createCardToken({


            cardNumber: numCartao,
            cvv: cvvCartao,
            expirationMonth: expiracaoMes,
            expirationYear: expiracaoAno,

            success: function(response)


            {  


              $("#tokenPagamentoCartao").val(response['card']['token']);


             identificador = PagSeguroDirectPayment.getSenderHash();
             $(".hashPagSeguro").val(identificador);


              numCartao = $("#cardNumber").val();
              numCartao = numCartao.replace(" ", "");
             cvvCartao = $("#cvv").val();
             expiracaoMes = $("#mes").val();
             expiracaoAno = $("#ano").val();



             }});

                 $( "#pagamento" ).hide( "blind", { direction: "down" }, "slow" );
                   $( "#dados_pagamento" ).show( "blind", { direction: "down" }, "slow" );

             return false; // required to block normal submit since you used ajax




        }
    });
});
